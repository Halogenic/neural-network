
"""
Training a neural network with GAs http://ijcai.org/Past%20Proceedings/IJCAI-89-VOL1/PDF/122.pdf
"""

import math
import random

from collections import defaultdict


class NeuralNetError(Exception):
	pass


class Neuron(object):
	
	def __init__(self, bias=0):
		self.bias = bias
		self.state = 0

	def activate(self, input_value):
		"""
		Uses a sigmoidal activation function
		"""

		# update the state
		self.state = (1 / (1 + math.exp(-input_value))) + self.bias

	def __str__(self):
		return str(self.bias)

	def __repr__(self):
		return "Neuron(bias=%s)" % self.bias


class Connection(object):
	"""
	Represents a directed connection between Neurons.
	"""

	def __init__(self, start, end, weight=0.5):
		self.start = start
		self.end = end

		self.weight = weight


class Network(object):

	def __init__(self):
		self._connections_out = defaultdict(list)
		self._connections_in = defaultdict(list)

		self._input_layer = None
		self._output_layer = None
		self._hidden_layers = []


	@property
	def layers(self):
		return [self._input_layer] + self._hidden_layers + [self._output_layer]


	def topology(self):
		return [len(self._input_layer)] + [len(layer) for layer in self._hidden_layers] + [len(self._output_layer)]


	def get_neuron(self, layer, index):
		return self.layers[layer][index]


	def get_connections_out(self, neuron):
		return self._connections_out[neuron]


	def get_connections_in(self, neuron):
		return self._connections_in[neuron]


	def add_input_layer(self, size):
		layer = [Neuron() for _ in xrange(size)]

		self._input_layer = layer


	def add_output_layer(self, size):
		layer = [Neuron() for _ in xrange(size)]

		self._output_layer = layer


	def add_hidden_layer(self, size):
		layer = [Neuron() for _ in xrange(size)]

		self._hidden_layers.append(layer)


	def add_connection(self, start, end):
		conn = Connection(n1, n2)
		self._connections_out[n1].append(conn)
		self._connections_in[n2].append(conn)
		

	def connect(self):
		"""
		Connect layers together automatically.
		"""

		layers = self.layers

		for i, layer in enumerate(layers):
			# don't connect output layer
			if layer != layers[-1]:
				next_layer = layers[i + 1]

				for n1 in layer:
					for n2 in next_layer:
						self.add_connection(n1, n2)

	def randomize(self, weight_range, bias_range=None):
		"""
		Randomize the weights associated with each connection and 
		the biases on each of the neurons.
		"""

		weight_lo, weight_hi = weight_range if weight_range else (0, 0)
		bias_lo, bias_hi = bias_range if bias_range else (0, 0)

		for connections in self._connections_out.itervalues():
			for connection in connections:
				connection.weight = weight_lo + (random.random() * (weight_hi - weight_lo))

		for layer in self.layers:
			for neuron in layer:
				neuron.bias = bias_lo + (random.random() * (bias_hi - bias_lo))


	def activate(self, inputs):
		if not self._input_layer:
			raise NeuralNetError("Must have an input layer")

		if not self._output_layer:
			raise NeuralNetError("Must have an output layer")

		if len(inputs) != len(self._input_layer):
			raise NeuralNetError("Inputs list must have same size as input layer")

		# set the output energy of the input neurons to match the inputs
		for neuron, energy in zip(self._input_layer, inputs):
			neuron.state = energy

		layers = self.layers

		# propagate the activation energies through the network
		for layer in layers[:-1]:
			for neuron in layer:
				try:
					connections = self._connections_out[neuron]
				except KeyError:
					continue

				for connection in connections:
					connection.end.activate(neuron.state * connection.weight)

		return [n.state for n in self._output_layer]


# net = Network()

# net.add_input_layer(3)
# net.add_hidden_layer(5)
# net.add_output_layer(3)

# net.connect()
# net.randomize((0.0, 1.0), (0.0, 0.0))

# print net.activate([2, 4, 7])
