
"""
Ideas from http://ijcai.org/Past%20Proceedings/IJCAI-89-VOL1/PDF/122.pdf
"""

import random

from weakref import WeakKeyDictionary

from neuralnet import Network


# class PingPongNumber(object):
# 	"""
# 	A descriptor for storing a 'ping-pong number', this is a 
# 	number that doesn't wrap around when over- or under-flowing
# 	and instead 'bounces' its value.

# 	e.g. instead of 0.9 + 0.2 = 0.1  ===>  0.9 + 0.2 = 0.9 (bounce back from 1.0)
# 	"""

# 	def __init__(self, lo, hi):
# 		self.lo = lo
# 		self.hi = hi

# 		self.data = WeakKeyDictionary()

# 	def __get__(self, instance, owner):
# 		return self.data[instance]

# 	def __set__(self, instance, value):
# 		if value > self.hi:
#             value = abs(-self.hi + (abs(value) % self.hi))
#         elif value < self.lo:
#             value = -self.lo - (abs(value) % self.)

# 		self.data[instance] = value


class Solution(object):
	"""
	A neural network solution.

	Encodes its network in a one-dimensional list of numbers. Each neuron
	is represented in order from the output neurons to the first hidden layer.
	With input links given before the neuron bias, with each neuron from left to right.
	"""

	def __init__(self, inputs, hidden, outputs):
		self.inputs = inputs
		self.hidden = hidden
		self.outputs = outputs

		self.genome = []

		self._network = None


	def randomize(self):
		# network is fully connected

		# combine all layer counts into one list
		layers = [self.inputs] + self.hidden + [self.outputs]

		# construct genome wtih all layer counts
		for layer_index, layer_count in reversed([(i + 1, count) for i, count in enumerate(layers[1:])]):
			# get all connections, given by layer before this one
			conns = layers[layer_index - 1]

			# for each neuron in this layer
			# get random connection weights and biases
			for n in xrange(layer_count):
				for i in xrange(conns):
					# get random connection weight
					self.genome.append(random.random() * 2 - 1)

				# get random bias
				self.genome.append(random.random() * 2 - 1)


	def network(self):
		if not self._network:
			# create the network from the encoded genome
			# we are assuming the network is fully connected

			self._network = Network()
			self._network.add_input_layer(self.inputs)
			for size in self.hidden:
				self._network.add_hidden_layer(size)
			self._network.add_output_layer(self.outputs)

			# connect all layers together
			self._network.connect()

			layers = [self.inputs] + self.hidden + [self.outputs]

			genome_index = 0

			for layer_index, layer_count in reversed([(i + 1, count) for i, count in enumerate(layers[1:])]):
				# for each neuron in this layer
				for n in xrange(layer_count):
					neuron = self._network.get_neuron(layer_index, n)
					connections = self._network.get_connections_in(neuron)

					for conn in connections:
						conn.weight = self.genome[genome_index]
						genome_index += 1

					neuron.bias = self.genome[genome_index]
					genome_index += 1

		return self._network


class Population(object):

	def __init__(self, inputs, hidden, outputs, size=10):
		self.inputs = inputs
		self.hidden = hidden
		self.outputs = outputs

		self.size = size

		self.solutions = []


	def init(self):
		# create random initial population
		for i in xrange(self.size):
			solution = Solution(self.inputs, self.hidden, self.outputs)
			solution.randomize()
			self.solutions.append(solution)


	def select(self, count=2):
		pass


	def crossover(self, selected):
		pass


	def mutate(self, selected):
		pass


	def __iter__(self):
		return iter(self.solutions)


class TrainerGA(object):
	
	def __init__(self, inputs, hidden, outputs):
		self.inputs = inputs
		self.hidden = hidden
		self.outputs = outputs


	def train(self, data):
		pass


import unittest


class SolutionTests(unittest.TestCase):

	def setUp(self):
		self.solution = Solution(3, [2], 1)




def main():
	solution = Solution(3, [2], 1)
	solution.randomize()
	print solution.genome
	print solution.network().layers


if __name__ == "__main__":
	main()